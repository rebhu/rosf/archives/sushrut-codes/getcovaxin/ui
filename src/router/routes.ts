import { RouteConfig } from 'vue-router';

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'privacy', component: () => import('pages/Privacy.vue') },
      { path: 'tos', component: () => import('pages/ToS.vue') },
      { path: 'mass-effect/c-sec/admin-panel', component: () => import('pages/AdminPanel.vue') },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
];

export default routes;
